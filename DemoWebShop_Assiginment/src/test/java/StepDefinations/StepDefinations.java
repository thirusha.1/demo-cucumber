package StepDefinations;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.google.common.io.Files;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class StepDefinations {
	static WebDriver driver;

	@Given("user should naviagate to the applications")
	public void user_should_naviagate_to_the_applications() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Given("user click the login buttons")
	public void user_click_the_login_buttons() {
		driver.findElement(By.partialLinkText("Log in")).click();

	}

	@Given("user enter user name {string}")
	public void user_enter_user_name(String string) {
		driver.findElement(By.name("Email")).sendKeys(string);

	}

	@Given("user enter passowords {string}")
	public void user_enter_passowords(String string) {
		driver.findElement(By.name("Password")).sendKeys(string);
		

	}

	@When("user click the login button")
	public void user_click_the_login_button() {
		driver.findElement(By.xpath("//input[@class=\"button-1 login-button\"]")).click();
		 
	}

	@Then("to verfied the login page")
	public void to_verfied_the_login_page() {
		WebElement asserts = driver.findElement(By.partialLinkText("manzmehadi1@gmail.com"));
		String text = asserts.getText();
		Assert.assertEquals(text, "manzmehadi1@gmail.com");
	}

	@When("user click the book in catogeriese filed")
	public void user_click_the_book_in_catogeriese_filed() {
		driver.findElement( By.partialLinkText("Books")).click();
		
	}

	@When("Sort the books in High to Low")
	public void sort_the_books_in_high_to_low() {
		WebElement product = driver.findElement(By.id("products-orderby"));
		Select s=new Select(product);
		s.selectByIndex(4);
		
		
	}
	@When("Add Two product into Cart")
	public void add_two_product_into_cart() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,500)");
		
		driver.findElement(By.xpath("(//input[@type=\"button\"])[4]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//input[@type=\"button\"])[5]")).click();

		
	}

	@When("user click the Electronic in catogeries filed")
	public void user_click_the_electronic_in_catogeries_filed() throws InterruptedException {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,-500)");
		Thread.sleep(3000);

		
		driver.findElement(By.partialLinkText("Electronics")).click();
		
	}

	@When("user select cell phone")
	public void user_select_cell_phone() {
		driver.findElement(By.xpath("//img[@alt=\"Picture for category Cell phones\"]")).click();
		
		
	}

	@When("click  Add product into Cart button")
	public void click_add_product_into_cart_button() {
		driver.findElement(By.xpath("(//input[@type=\"button\"])[3]")).click();
		
		//code bending
	}

	@When("user click the Gift category")
	public void user_click_the_gift_category() {
		driver.findElement(By.partialLinkText("Gift Cards")).click();
		
		
	}

	@When("user select  then it should Display {int} per page")
	public void user_select_then_it_should_display_per_page(Integer int1) {
		WebElement size = driver.findElement(By.id("products-pagesize"));
		Select sa=new Select(size);
		sa.selectByIndex(0);
		
	}

	@When("user  Capture the name and price of one of the  Gift cards displayed")
	public void user_capture_the_name_and_price_of_one_of_the_gift_cards_displayed() throws IOException {
		TakesScreenshot t=(TakesScreenshot)driver;
		File screenshotAs = t.getScreenshotAs(OutputType.FILE);
		File f =new File("/home/thirusha/Downloads/cucumber/DemoWebShop_Assiginment/src/test/resources/Captures/displyed.png");
		Files.copy(screenshotAs, f);
		
	}

	@When("user clikc the logout button")
	public void user_clikc_the_logout_button() {
		driver.findElement(By.partialLinkText("Log out")).click();
		
	}

}